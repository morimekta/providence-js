SRCS=$(wildcard src/main/**/*.java)
VERSION=$(shell git tag --merged | semver --max)
PVD_VERSION=$(shell cat pom.xml | grep '<providence\.version>' | sed -e 's:\s*</\?providence\.version>::g')

default:
	@echo " -- providence-js-$(VERSION) <-- pvd-$(PVD_VERSION)"
	@echo "make clean"
	@echo "make service"
	@echo "make package"
	@echo "make upload"

clean:
	mvn clean

target/js.jar: $(SRCS)
	mvn clean verify

package: target/js.jar

upload: package
	cp target/providence-js-$(VERSION).tar.gz           ../dl.morimekta.net/dl/archive
	cp target/providence-js-sandboxed-$(VERSION).tar.gz ../dl.morimekta.net/dl/archive
	cp target/providence-js-$(VERSION)_all.deb          ../dl.morimekta.net/dl/deb
	cp target/rpm/providence-js/RPMS/noarch/providence-js-$(VERSION).noarch.deb \
                                                        ../dl.morimekta.net/dl/rpm
	@$(eval SHA256SUM:=$(shell target/providence-js-sandboxed-$(VERSION).tar.gz | sha256sum | sed 's/ .*//'))
	cat src/homebrew/providence-js.rb | \
        sed -e 's/__VERSION__/$(VERSION)/' -e 's/__SHA256SUM__/$(SHA256SUM)/' -e 's/__PVD_VERSION__/$(PVD_VERSION)'\
	    > ../homebrew-tools/Formula/providence-js.rb

service: package
	pvd-generator --add-generator target/js.jar --gen js         -o src/main/resources/js          idl/gitlab.com/morimekta/providence-core/*.thrift
	pvd-generator --add-generator target/js.jar --gen js:closure -o src/main/resources/closure     idl/gitlab.com/morimekta/providence-core/*.thrift
	pvd-generator --add-generator target/js.jar --gen js:node    -o src/main/resources/node_module idl/gitlab.com/morimekta/providence-core/*.thrift
	pvd-generator --add-generator target/js.jar --gen js:ts      -o src/main/resources/type_script idl/gitlab.com/morimekta/providence-core/*.thrift

.PHONY: clean package service upload