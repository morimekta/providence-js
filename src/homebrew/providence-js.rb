class ProvidenceJs < Formula
    desc "Providence JS Generator"
    homepage "https://morimekta.net/providence"
    version "__VERSION__"
    url "https://dl.morimekta.net/archive/providence-js-sandboxed-#{version}.tar.gz"
    sha256 "__SHA256SUM__"

    depends_on :java => "1.8+"
    depends_on :providence => "__PVD_VERSION__+"

    def install
        inreplace "etc/providence/js.json",
                  "/share/providence",
                  "#{share}/providence"
        share.install Dir["share/*"]
        etc.install Dir["etc/*"]
    end
+end
