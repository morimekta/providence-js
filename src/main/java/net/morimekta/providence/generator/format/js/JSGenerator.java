/*
 * Copyright 2017 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.generator.format.js;

import net.morimekta.providence.generator.Generator;
import net.morimekta.providence.generator.GeneratorException;
import net.morimekta.providence.generator.GeneratorOptions;
import net.morimekta.providence.generator.format.js.formatter.JSProgramFormatter;
import net.morimekta.providence.generator.format.js.formatter.TSDefinitionFormatter;
import net.morimekta.providence.generator.util.FileManager;
import net.morimekta.providence.reflect.GlobalRegistry;
import net.morimekta.providence.reflect.ProgramRegistry;
import net.morimekta.providence.reflect.contained.CProgram;
import net.morimekta.util.Strings;
import net.morimekta.util.io.IOUtils;
import net.morimekta.util.io.IndentedPrintWriter;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;

/**
 * Generate JS message models for providence.
 *
 * Supports the providence styled JSON (both named and compact) as data
 * format.
 */
public class JSGenerator extends Generator {
    private final GeneratorOptions         generatorOptions;
    private final JSOptions                options;

    public JSGenerator(FileManager manager, GeneratorOptions generatorOptions, JSOptions jsOptions) throws GeneratorException {
        super(manager);

        this.options = jsOptions;
        this.generatorOptions = generatorOptions;
    }

    @Override
    @SuppressWarnings("resource")
    public void generate(ProgramRegistry registry) throws IOException, GeneratorException {
        CProgram program     = registry.getProgram();
        Path     programFile = Paths.get(registry.getProgram().getProgramFilePath());
        if (program.getNamespaceForLanguage("js") == null) {
            // just skip programs with missing js namespace.
            return;
        }

        String version = generatorOptions.program_version
                .replaceAll("-SNAPSHOT$", "");

        // Generate .d.ts definition files if typescript is enabled.
        if (options.type_script) {
            TSDefinitionFormatter tsdf = new TSDefinitionFormatter(options, registry);
            String fileName = tsdf.getFileName(program);
            String filePath = tsdf.getFilePath(program);
            getFileManager().createIfMissingOrOutdated(programFile, filePath, fileName, out -> {
                IndentedPrintWriter writer = new IndentedPrintWriter(out);

                writer.format("// Generated with %s %s", generatorOptions.generator_program_name, version)
                      .newline();

                tsdf.format(writer, program);

            });
        }

        JSProgramFormatter formatter = new JSProgramFormatter(options, registry);
        String fileName = formatter.getFileName(program);
        String filePath = formatter.getFilePath(program);
        getFileManager().createIfMissingOrOutdated(programFile, filePath, fileName, out -> {
            IndentedPrintWriter writer = new IndentedPrintWriter(out);

            writer.format("// Generated with %s %s", generatorOptions.generator_program_name, version)
                  .newline();

            formatter.format(writer, program);
        });
    }

    @Override
    public void generateGlobal(GlobalRegistry registry, Collection<Path> inputFiles) throws
                                                                                     IOException,
                                                                                     GeneratorException {
        boolean service = false;
        for (ProgramRegistry reg : registry.getRegistries()) {
            CProgram program = reg.getProgram();
            if (program.getServices().size() > 0) {
                service = true;
                break;
            }
        }

        if (service) {
            InputStream source;

            String targetPath = Strings.join(File.separator, "morimekta", "providence");
            if (options.node_js || options.type_script) {
                source = getClass().getResourceAsStream("/node_module/morimekta-providence/p_service.js");
                targetPath = "morimekta-providence";
            } else if (options.closure) {
                source = getClass().getResourceAsStream("/closure/morimekta/providence/p_service.js");
            } else {
                source = getClass().getResourceAsStream("/js/morimekta/providence/p_service.js");
            }

            getFileManager().createIfMissingOrOutdated(inputFiles.iterator().next(), targetPath, "p_service.js", out -> {
                IOUtils.copy(source, out);
            });

            if (options.type_script) {
                getFileManager().createIfMissingOrOutdated(inputFiles.iterator().next(), targetPath, "p_service.js", out -> {
                    IOUtils.copy(getClass().getResourceAsStream("/type_script/morimekta-providence/p_service.d.ts"), out);
                });
            }
        }

    }
}
